# Grammar Check using NLP

Grammar Check in user reviews dataset based on negative ratings.

## Getting started

Remove stopwords from the data - As we know stop words are most common words in any language and does'nt add much information to the text so we need to remove them from the text.

-remove punctuation from the text
-remove accented characters into english characters
-treat mispelled words
-preprocessing of the text
-Next, read the data from review_data.csv file and apply preprocessing on the csv file.
-Then save the preprocessed text in the file processed_reviews_data.csv.

Used language_tool_python-  to check text data row wise for the grammatical correctness of the text.

Number of issues in the sentence is 0 this implies sentence is grammatically correct and greater than zero implies the number of issues in the sentence.

Used swifter for faster implementation using parallel processing.
Libraries used: pyspellchecker language-tool-python swifter

## Note:
In file upload, it is mandatory to upload the review_data.csv.
